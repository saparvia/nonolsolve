.. highlight:: lhs

::

> {-# LANGUAGE FlexibleInstances, TypeSynonymInstances #-}
> module Main where

==========
Nonolsolve
==========

Usage
-----

* Compilation: ``cabal build``
* Usage: ``dist/build/nononsolve/nonolsolve < input.txt``


Imports
-------

Let's start by importing some required modules.

::

> import Data.List (transpose, group, groupBy, intersperse, intercalate, find, elemIndex, findIndices)
> import Text.Regex.Applicative (sym, many, some, (*>), (<|>), (=~))
> import Data.Maybe (fromJust, isJust, isNothing, fromMaybe, listToMaybe)
> import Data.Bifunctor (bimap, first)
> import Data.Function (on)

Main program
------------

Nonosolve is a fairly simple program. Just read the nonogram, feed it to the solver and the print the (hopefully) solved nonogram to stdout.

::

> main :: IO ()
> main = parseInput >>= (printBoard . solve)


The parsing code is very simple.

The column hints are given on the first line, and hints for the rows are given on the second line. Hints for the same line are separated by "," and lines are separated by ";". Thus the following is a possible input:

.. code-block:: text

    4;3;4;1;1
    1;3,1;4;1,1;1,1

::

> parseInput :: IO Board
> parseInput = do
> 	colHintStr <- getLine
> 	rowHintStr <- getLine
> 	return $ emptyBoard (stringToHints rowHintStr) (stringToHints colHintStr)
> 	where
> 		stringListToIntList = map (map read)
> 		splitString str = map (split ',') (split ';' str)
> 		stringToHints = stringListToIntList . splitString

Print a board in a human-readable format

::

> printBoard :: Board -> IO()
> printBoard (cells, _) = mapM_ print cells

Datatypes
---------

Before going any further it's probably a good idea to take a look at the datatypes used.

A trit is like a bit except that it also has an additional "unknown" state. We implement this is a ``Maybe Bool``, which means we can also take advantage of the power of the ``Maybe`` monad. Here we also define how to display trits when printing them. Chosing values resembling the traditional ``0``, ``1`` boolean values might be a good idea in a more general case, but the choice made here is based on readability of the solved nonogram.

::

> type Trit = Maybe Bool
> true3 = Just True
> false3 = Just False
> unknown3 = Nothing
>
> instance {-# OVERLAPPING #-} Show Trit where
> 	show (Just False) = " "
> 	show (Just True) = "X"
> 	show Nothing = "?"
> 	showsPrec _ x s = show x ++ s
> 	showList [] = showString ""
> 	showList (x:xs) = shows x . shows xs

Trinary negation is identical to the normal boolean version, except that a unknown value stays unknown.

::

> not3 :: Trit -> Trit
> not3 = fmap not

In addition to the trit datatype we define a few more types for conveniance.

As we see, a line is defined as a tuple containing a list of trits ("the cells") and  the hints corresponding that line. A board on the other hand is a tuple containing a matrix of cells and a collection of hints. The board could also have been implemented as a list of lines, but that would have created some difficulties later on.

::

> type Hint = Int
> type Hints = [Hint]
> type Line = ([Trit], Hints)
> type Board = ([[Trit]], ([Hints], [Hints]))

Solver
------

The solver starts by doing an inital pass, applying some very "cheap" rules, before starting with the more complicated and expensive rules. After the inital solve the solver starts solving "easy" lines, i.e lines with a lot of hints, and then moves on to increasingly more difficult lines. This method sometimes increases efficiency considerably. Some solver rules require huge amounts of memory when applied to lines with few cells already solved. It is therefore a good idea to solve as much as possible of the nonogram before moving on to those "hard" lines.

For small nonograms these extra passes however sometimes decrease efficiency. Doing benchmarks to test different settings (e.g starting with more difficult lines then the very simplest) would probably be a good idea...

::

> solve :: Board -> Board
> solve board = (fromJust .: find) isComplete $ iterateWithCounter solveStep (initalSolve board) 1 (+1)

The inital solve currently only consists of identifying lines that require no solving. Not sure what else could be done at this point.

::

> initalSolve :: Board -> Board
> initalSolve = applyRule onlyOneTypeLeft

Try to solve the board, but only apply the expensive "brute-force" approach on lines which have enough hints. By applying this function repeatedly but increasing the threshold on each pass guarantees that "sane" amounts of memory is used at any time.

Since we really want to solve as much as possible using the cheap rules we apply them repeatedly until no further solving can be done. Only after that is the more expensive rule applied.

::

> solveStep :: Board -> Int -> Board
> solveStep board threshold = applyRule (cheapRules . selectiveBruteLine threshold . cheapRules ) board
> 	where
> 		cheapRules = lastNonRepeating . iterate onlyOneTypeLeft . completeLineEnds . tryOne . applyBothDirections patternWontFitStart . 
>			onlyOneTypeLeft . completeLineEnds . applyBothDirections patternCanStartEarliest . onlyOneTypeLeft . completeLineEnds
> 		completeLineEnds = applyBothDirections completeLineStart

Apply the given rule to every row and column in the board.

::

> applyRule :: (Line -> Line) -> Board -> Board
> applyRule p board@(_, hints) = applyCols (applyRows board)
> 	where
> 		applyRows board' = (map (fst . p) (boardRows board'), hints)
> 		applyCols board' = ((transpose . (map (fst . p))) (boardCols board'), hints)

Several rules work only work on the start of a line. By applying those rules in both directions the end of the lines also get solved.

::

> applyBothDirections :: (Line -> Line) -> Line -> Line
> applyBothDirections p = reverseLine . p . reverseLine . p

Solver rules
************

These are the actual rules used for solving the nonogram. Several lower-level functions which work on lines are used here, so it might be a good idea to take a quick look at them if these rules seem confusing.

Generate every possible solution for the given line, and check which cells values are common in every variation. Those values which are the same in every variation must also be part of the correct solution for the line.

Note that for long lines the there is a large number of possible variations which means that a lot of memory is used. Bacause of this the solver should always try to solve as much as possible using other methods.

*Before:*

.. image:: images/brute_pre.svg
    :alt: 8|?|?|?|?|?|?|?|?|?|?|

*After:*

.. image:: /images/brute_post.svg
    :alt: 8|?|X|X|X|X|X|X|X|X|?|

::

> bruteLine :: Line -> Line
> bruteLine line = reassembleLine line (cellUnion (fst (lineIntersection permutations)) (fst line'))
> 	where
> 		line' = incompleteMiddle line
> 		permutations = permuteLine line'

Because the brute-force method is so expensive it is only recommended to use it on lines that only have few variations, or are already partially solved. This function checks that the line has "enough" (defined by the given threshold) hints to be solved easily before actually trying to solve the line.

::

> selectiveBruteLine :: Int -> Line -> Line
> selectiveBruteLine threshold line@(cells, hints) = if sum hints > minHints then bruteLine line else line
> 	where
>		minHints = length cells - threshold

Once all the patterns given by the hints have been found it is clear that all the remaining unknown cells should be empty. Similarly, if there are exactly the same number of unknown cells as there are missing filled (as given by the hints) it is clear that all the remaining unknown cells should actally be filled.

*Before:*

.. image:: images/onlyone_pre.svg
    :alt: 2,3|?|X|X|?|?|X|X|X|?|?|

*After:*

.. image:: images/onlyone_post.svg
    :alt: 2,3| |X|X| | |X|X|X| | |

::

> onlyOneTypeLeft :: Line -> Line
> onlyOneTypeLeft line@(cells, hints) = (first (onlyFalse3Left . onlyTrue3Left)) line
> 	where
> 		filled = count true3 cells
> 		notSure = count Nothing cells
> 		onlyFalse3Left cells' = if filled == sum hints then replace unknown3 false3 cells' else cells'
> 		onlyTrue3Left cells' = if notSure == sum hints - filled then replace unknown3 true3 cells' else cells'

If a few cells at the beginning of a line is known then it can be extended to fit the given hint.

*Before:*

.. image:: images/completestart_pre.svg
    :alt: 5,2|X|X|X|?|?|?|?|?|?|?|

*After:*

.. image:: images/completestart_post.svg
    :alt: 5,2|X|X|X|X|X| |?|?|?|?|

::

> completeLineStart :: Line -> Line
> completeLineStart = lastNonRepeating . (iterate completeLineStart')

> completeLineStart' :: Line -> Line
> completeLineStart' line@(cells, hints) = (first new) line
> 		where
> 			new = replaceStart (start ++ replicate missingFromLastPattern true3 ++ separator)
> 			separator = if (missingFromLastPattern /= 0 || (not (null start) && (last start == true3))) && (length start + missingFromLastPattern /= length cells) then [false3] else []
> 			patternsAtStart = length $ patternLengths start
> 			missingFromLastPattern
>				| patternsAtStart == 0 || last start == false3 = 0
>				| otherwise = (hints!!(patternsAtStart-1)) - last (patternLengths start)
> 			start = fst $ completeStart line
> 			patternLengths cells' = map length $ filter ((true3 ==).head) (group cells')

If the first pattern given by the hints wouldn't fit in front of the first pattern found in the line it is clear that the found pattern must also be the first pattern given by the hints. By checking the position of the last known cell of the found pattern it is easily calculated how far back the pattern can extend at most (it could of course also extend in the other direction). Any cells before the calculated maximum must be empty (otherwise the found pattern could not be the first pattern as was previously established).

*Before:*

.. image:: images/minstart_pre.svg
    :alt: 3,2|?|?|?|X|X|?|?|?|?|?|

*After:*

.. image:: images/minstart_post.svg
    :alt: 3,2| | |?|X|X|?|?|?|?|?|

::

> patternCanStartEarliest :: Line -> Line
> patternCanStartEarliest line = (first new) line
> 	where
> 		new _ = start ++ if ((fromMaybe 0) . listToMaybe) hints' < firstFilled then cells' else replaceStart (replicate newFalse3AtBeginning false3) cells'
> 		cells' = fst $ incompleteEnd line
> 		hints' = snd $ incompleteEnd line
> 		newFalse3AtBeginning = endOfFirstPattern + 1 - ((fromMaybe 0) . listToMaybe) hints'
> 		firstFilled = length (takeWhile (/=true3) cells')
> 		endOfFirstPattern = firstFilled + length (takeWhile (==true3) (dropWhile (/=true3) cells')) - 1
> 		start = fst $ completeStart line

If the first pattern won't fit before the first known empty cell it's clear that all the cells before the known empty one must also be empty.

*Before:*

.. image:: images/wontfitstart_pre.svg
    :alt: 3,2|?|?| |?|?|?|?|?|?|?|

*After:*

.. image:: images/wontfitstart_post.svg
    :alt: 3,2| | | |?|?|?|?|?|?|?|

::

> patternWontFitStart :: Line -> Line
> patternWontFitStart line = (first new) line
> 	where
> 		new _ = start ++ if fmap (\i-> head hints' <= i) firstFalse3 /= Just False then cells' else replaceStart (replicate (fromJust firstFalse3) false3) cells'
> 		cells' = fst $ incompleteEnd line
> 		hints' = snd $ incompleteEnd line
> 		firstFalse3 = elemIndex false3 cells'
> 		start = fst $ completeStart line

See what happens if we fill in one square. If it leads to a conflict then we know that the actual value is the opposite.

*Before:*

.. image:: images/tryone_pre.svg
    :alt: 2,2|?|?|X|X|?|?|?|?|?|?|

*After:*

.. image:: images/tryone_post.svg
    :alt: 2,2|?| |X|X| |?|?|?|?|?|

::

> tryOne :: Line -> Line
> tryOne line@(_, hints) = (first new) line
> 	where
> 		new = (tryReplace false3 . tryReplace true3)
> 		tryReplace value list = map (\(i, e) -> if isNothing e && (not $ lineIsPossible $ (replaceAt list i value, hints)) then not3 value else e) (withIndex list)
>		replaceAt xs i x = take i xs ++ [x] ++ drop (i + 1) xs


Board functions
---------------

Create board with every cell unknown

::

> emptyBoard :: [Hints] -> [Hints] -> Board
> emptyBoard rowHints colHints = (board, (rowHints, colHints))
> 	where
> 		board = replicate2d width height unknown3
> 		width = length colHints
> 		height = length rowHints

Check if the board is completely solved

::

> isComplete :: Board -> Bool
> isComplete board = all lineIsComplete (boardRows board)

Get the row-lines of the given board

::

> boardRows :: Board -> [Line]
> boardRows (cells, (rowHints, _)) = zip cells rowHints

Get the column-lines of the given board

::

> boardCols :: Board -> [Line]
> boardCols (cells, (_, colHints)) = zip (transpose cells) colHints

Line functions
--------------

Check if a line is fully known

::

> lineIsComplete :: Line -> Bool
> lineIsComplete (cells,_) = all isJust cells

Check if anything is known about a line

::

> lineIsUnknown :: Line -> Bool
> lineIsUnknown (cells,_) = all isNothing cells

Check whether a line is consistent with the given hints

::

> lineIsPossible :: Line -> Bool
> lineIsPossible (cells, hints) = isJust $ cells =~ (start *> middle *> end)
> 	where
> 		start = many (sym false3 <|> sym unknown3)
> 		end = start
> 		middle = foldr (*>) (pure []) (intersperse sep (map pat hints))
> 		pat n =  ntimes n (sym true3 <|> sym unknown3)
> 		sep = some (sym false3 <|> sym unknown3)
> 		ntimes n regex = foldr (*>) (pure []) (replicate n regex)

Create permutations of a line by moving filled cells around

::

> permuteLine :: Line -> [Line]
> permuteLine ([],_) = []
> permuteLine line@(_, hints) = filter (matchesLine line) (foldr concatShifts [baseLine line] [hintCount, hintCount - 1 .. 0])
> 	where
> 		hintCount = length hints
> 		concatShifts n = concatMap (allShifts n)

Check which cells are the same in multiple lines

::

> lineIntersection :: [Line] -> Line
> lineIntersection lines' = ((elemIntersection (map fst lines')), head (map snd lines'))
> 	where
> 		elemIntersection xs = map (ifThenElse allSame head (`seq` Nothing)) (transpose xs)

Take the union of multiple lines

::

> cellUnion :: [Maybe a] -> [Maybe a] -> [Maybe a]
> cellUnion = zipWith firstIfJust

Check if two lines are the same, ignoring unknowns

::

> matchesLine :: Line -> Line -> Bool
> matchesLine = (and .: (zipWith maybeEq)) `on` fst

Create a possible line which matches the hints

::

> baseLine :: Line -> Line
> baseLine line@(cells, hints) = (first new) line
> 	where
>		new _ = pad false3 width patterns
> 		patterns = concatMap (\n->append false3 (replicate n true3)) hints
> 		width = length cells

Reverse a line

::

> reverseLine :: Line -> Line
> reverseLine = bimap reverse reverse

Get length of a line

::

> lineLen :: Line -> Int
> lineLen = (fst .: first) length

Remove a known pattern from the line

::

> dropPattern :: Int -> [Trit] -> [Trit]
> dropPattern n cells = (iterate (dropWhile (==true3) . dropWhile (/=true3)) cells)!!n

Shift a pattern forward in the line by one step

::

> shiftPat :: Int -> [Trit] -> [Trit]
> shiftPat n cells
> 	| last cells == true3 = cells
>	| otherwise = dropEnd (length t) cells ++ false3:init t
> 		where
> 			t = dropPattern n cells

Shift a pattern forward in the line

::

> shiftSteps :: Int -> Int -> [Trit] -> [Trit]
> shiftSteps n steps cells = (iterate (shiftPat n) cells)!!steps

Generate possible lines obtained by shifting a pattern forward

::

> allShifts :: Int -> Line -> [Line]
> allShifts n (cells, hints) = map (\x->(shiftSteps n x cells, hints)) [0 .. maxShifts]
> 	where
> 		maxShifts = length $ takeWhile (==false3) (reverse cells)

Return a new line with just the known start of another line

::

> completeStart :: Line -> Line
> completeStart line@(cells, _) = bimap (takeWhile isJust) (take completePatterns) line
> 	where
> 		completePatterns = count true3 (collapse (takeWhile isJust cells))

Return a new line with just the known end of another line

::

> completeEnd :: Line -> Line
> completeEnd = reverseLine . completeStart . reverseLine

Return a new line with just the unknown end of another line

::

> incompleteEnd :: Line -> Line
> incompleteEnd line = bimap (drop (length $ fst start)) (drop (length $ snd start)) line
> 	where
> 		start = completeStart line

Return a new line with just the unknown middle of another line

::

> incompleteMiddle :: Line -> Line
> incompleteMiddle line = bimap (dropEnd (length $ fst end)) (dropEnd (length $ snd end)) end'
> 	where
> 		end' = incompleteEnd line
> 		end = completeEnd line

Take some cells and replace the previously unknown part in a line with them

::

> reassembleLine :: Line -> [Trit] -> Line
> reassembleLine original [] = original
> reassembleLine original@(_, hints) newMiddle = (fst (completeStart original) ++ newMiddle ++ fst (completeEnd original), hints)

General functions
-----------------

The Haskell standard library is somewhat limited, so we need to define some general functions here that are used later in the program.
Most of these would probably be somewhat useful in other program, so it might be a good idea to move them to an external library at some point.

::

> countBy :: Eq a => (a->Bool) -> [a] -> Int
> countBy = length .: filter

::

> count :: Eq a => a -> [a] -> Int
> count = countBy . (==)

::

> ifThenElse :: (a->Bool) -> (a->b) -> (a->b) -> a -> b
> ifThenElse p f g x = if p x then f x else g x

::

> (.:) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
> (f .: g) x y = f (g x y)
> infixr 8 .:

::

> replicate2d :: Int -> Int -> a -> [[a]]
> replicate2d rows cols = (replicate rows) . (replicate cols)

::

> append :: a -> [a] -> [a]
> append x xs = xs ++ [x]

::

> allSame :: Eq a => [a] -> Bool
> allSame = (<= 1) . length . group

::

> maybeEq :: Eq a => Maybe a -> Maybe a -> Bool
> maybeEq Nothing _ = True
> maybeEq _ Nothing = True
> maybeEq x y = x == y

::

> firstIfJust :: Maybe a -> Maybe a -> Maybe a
> firstIfJust x _ | isJust x = x
> firstIfJust _ y = y

The standard prelude includes functions to split a string at spaces, but not by an arbitrary character. I really wonder why they didn't include a more general split function. Maybe I'm missing something?
Anyway, this split function does exactly what I want. It takes a list and a delimiter element, and splits the list into multiple parts. The delimiter element is NOT included in the result.

::

> split :: Eq a => a -> [a] -> [[a]]
> split _ [] = [[]]
> split delim (x:xs)
> 	| x == delim = [] : rest
> 	| otherwise = (x : head rest) : tail rest
> 	where
> 		rest = split delim xs

Append elements to the end of a list until it reaches the specified length.

::

> pad :: a -> Int -> [a] -> [a]
> pad c n list = take n (list ++ repeat c)

Remove all adjacent elements matching p except the first one

::

> collapseBy :: Eq a => (a->a->Bool) -> [a] -> [a]
> collapseBy _ [] = []
> collapseBy p list = map head (groupBy p list)

Remove all identical adjacent elements except the first one

::

> collapse :: Eq a=> [a] -> [a]
> collapse = collapseBy (==)

Replace occurances in list

::

> replace :: Eq a => a -> a -> [a] -> [a]
> replace orig repl = map (\x->if x==orig then repl else x)

Replace the start of a list with contents of another list

::

> replaceStart :: [a] -> [a] -> [a]
> replaceStart repl original = repl ++ drop (length repl) original

Take elements from the list until two identical are found next to each other

::

> takeUntilRepeat :: Eq a => [a] -> [a]
> takeUntilRepeat [] = []
> takeUntilRepeat [x] = [x]
> takeUntilRepeat (x:y:xs)
>     | x==y = [x]
>     | otherwise = x : takeUntilRepeat (y:xs)

Drop the n last elements

::

> dropEnd :: Int -> [a] -> [a]
> dropEnd n = reverse . drop n . reverse

Take last element before repeat

::

> lastNonRepeating :: Eq a => [a] -> a
> lastNonRepeating = last . takeUntilRepeat

Sometimes a function in a loop need to know how many times it has been called (like a "for-loop"). This function is similar to iterate, except that it also takes a start value for the counter and a function which modifies the counter at each loop.

::

> iterateWithCounter :: (a -> b -> a) -> a -> b -> (b -> b) -> [a]
> iterateWithCounter f x start step = map fst $ iterate (\(result, i)-> f' (result, i)) (x, start)
> 	where
> 		f' (x', i') = (f x' i', step i')

::

> withIndex :: [a] -> [(Int, a)]
> withIndex = zip [0..]
