Nonolsolve is a program that solves nonograms (see e.g. http://en.wikipedia.org/wiki/Nonogram).

It takes as input the nonogram hints and prints the solved nonogram on stdout.

Nonolsolve is written in Literate Haskell. A prettified version can be found at https://saparvia.gitlab.io/nonolsolve
