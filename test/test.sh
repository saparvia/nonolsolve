#!/bin/sh

echo -n "Solving 5x5..."
cat "test/5x5.txt" | `which time` -p ./nonolsolve >out.txt
diff -q out.txt "test/5x5-solution.txt" >> /dev/null
if [ "$?" -ne "0" ]; then
	echo "Fail"
	exit 1
else
	echo "Success"
fi

echo -n "Solving 10x10..."
cat "test/10x10.txt" | `which time` -p ./nonolsolve >out.txt
diff -q out.txt "test/10x10-solution.txt" >> /dev/null
if [ "$?" -ne "0" ]; then
	echo "Fail"
	exit 1
else
	echo "Success"
fi

echo -n "Solving 25x25..."
cat "test/25x25.txt" | `which time` -p ./nonolsolve >out.txt
diff -q out.txt "test/25x25-solution.txt" >> /dev/null
if [ "$?" -ne "0" ]; then
	echo "Fail"
	exit 1
else
	echo "Success"
fi
echo -n "Solving 30x30..."
cat "test/30x30.txt" | `which time` -p ./nonolsolve >out.txt
diff -q out.txt "test/30x30-solution.txt" >> /dev/null
if [ "$?" -ne "0" ]; then
	echo "Fail"
	exit 1
else
	echo "Success"
fi

echo -n "Solving 50x50..."
cat "test/50x50.txt" | `which time` -p ./nonolsolve >out.txt
diff -q out.txt "test/50x50-solution.txt" >> /dev/null
if [ "$?" -ne "0" ]; then
	echo "Fail"
	exit 2
else
	echo "Success"
fi
echo "Done."
